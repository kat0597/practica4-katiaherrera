package clases;

public class Hotel {

	// Atributo
	private Reservas[] reservas;

	// Constructor
	public Hotel(int numHabitaciones) {

		this.reservas = new Reservas[numHabitaciones];

	}
	
	
	//Damos de alta a un nuevo cliente
	
	public void altaHospedado(String nombreReserva, String apellido, double precio, int numHabitacion){
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] == null) {
				reservas[i] = new Reservas(nombreReserva);
				reservas[i].setApellidos(apellido);
				reservas[i].setPrecioHabitacion(precio);
				reservas[i].setNumHabitacion(numHabitacion);
				break;
			}
			
			
		}
		
	}
	
	
	//Metodo para buscar a un hospedado
	
	public Reservas buscarReserva(String nombreReserva) {
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] != null) {
				//A�adimos el ignore Case para que despues no haya problemas
				if (reservas[i].getNombreReserva().equalsIgnoreCase(nombreReserva)) {
					return reservas[i];
				}
			}
		}
		return null;
	}
	
	
	//Vamos a proceder a eliminar una reserva
	
	public void eliminarReserva(String nombreReserva) {
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] != null) {
				//A�adimos el ignore Case para que despues no haya problemas
				if (reservas[i].getNombreReserva().equalsIgnoreCase(nombreReserva)) {
					reservas[i]=null;
				}
			}
		}
	}
	
	
	//Ahora vamos a listar todos los objetos de arrays que esten completados
	
	public void listaReservas() {
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] != null) {
					System.out.println(reservas[i]);
				
			}
		}
	}
	
	
	//Cambiamos una reserva que ya estaba creada
	
	public void cambiarReserva(String nombreReserva, String reserva2) {
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] != null) {
				if (reservas[i].getNombreReserva().equalsIgnoreCase(nombreReserva)) {
					reservas[i].setNombreReserva(reserva2);
				}
			}
		}
	}
	
	
	//Listamos por rango de precios de las habitaciones
	
	public void listarPorRangoDePrecios(int precioBajo, int precioAlto) {
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] != null) {
				if (reservas[i].getPrecioHabitacion() < precioAlto && reservas[i].getPrecioHabitacion() > precioBajo) {
					
					System.out.println(reservas[i].toString());
					
				}
			}
		}
	}
	
	//Cambiar el numero de habitacion del hospedado
	
	public void cambiarHabitacion(String nombreReserva, int numHabitacion) {
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] != null) {
				if (reservas[i].getNombreReserva().equalsIgnoreCase(nombreReserva)) {
					reservas[i].setNumHabitacion(numHabitacion);
				}
			}
		}
	}
	
	//Incrementamos el 10% en aquellas habitaciones que sean pares
	public void incrementoDePrecio() {
		for (int i = 0; i < reservas.length; i++) {
			if (reservas[i] != null) {
				if (reservas[i].getNumHabitacion() %2 == 0) {
					reservas[i].setPrecioHabitacion(reservas[i].getPrecioHabitacion() * 1.1);
				}
			}
		}
	}
	
	//Nos muestra el numero de habitaciones que estan hospedadas en este momento
	
	public void numeroDeHabitaciones() {
		
		int contador = 0;
		for (int i = 0; i < reservas.length; i++) {
			if(reservas[i] != null) {
				if(reservas[i].getNumHabitacion() > 0) {
					contador ++;
				}
			}
		}
		
		System.out.println("El numero de habitaciones ocupadas es " + contador);
	}

}
