package clases;

public class Reservas {
	
	
	
	private String nombreReserva;
	private String apellidos;
	private double precioHabitacion;
	private int numHabitacion;
	
	//Constructor, que nos servira para inicializarlo mas tarde

	public Reservas(String nombreReserva) {
		this.nombreReserva = nombreReserva;
	}
	
	//Seter y Getter
	
	public String getNombreReserva() {
		return nombreReserva;
	}
	public void setNombreReserva(String nombreReserva) {
		this.nombreReserva = nombreReserva;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public double getPrecioHabitacion() {
		return precioHabitacion;
	}
	public void setPrecioHabitacion(double precioHabitacion) {
		this.precioHabitacion = precioHabitacion;
	}
	public int getNumHabitacion() {
		return numHabitacion;
	}
	public void setNumHabitacion(int numHabitacion) {
		this.numHabitacion = numHabitacion;
	}
	
	//Metodos to String
	
	public String toString(){
		return nombreReserva + " " + apellidos + " " + precioHabitacion + " " + numHabitacion;
	}

}
