package programa;

import java.util.Scanner;

import clases.Hotel;

public class Programa {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int op = 0;

		do {
			// Creamos un menu para que el usuario pueda escoger la opcion deseada
			System.out.println("Escoge una de las opciones");
			System.out.println("1 - Dar de alta a un hospedado");
			System.out.println("2 - Buscar una reserva");
			System.out.println("3 - Eliminar una reserva");
			System.out.println("4 - Listar las reservas");
			System.out.println("5 - Cambiar reserva");
			System.out.println("6 - Listar habitaciones por rango de precio");
			System.out.println("7 - Cambiar de habitacion");
			System.out.println("8 - Incrementar de precio (10%) en aquellas habitaciones que son par");
			System.out.println("9 - Mostrar el numero de habitaciones ocupadas");
			System.out.println("10 - Salir");

			// Damos el valor de la opcion a nuestra variable
			op = input.nextInt();

			// Creamos una instancia del numero de hospedados
			int hospedados = 10;
			Hotel hotelKat = new Hotel(hospedados);
			
			//Variables de los huepedes
			String nombre;
			String apellido;
			double precio;
			int numHabitacion;
			
			hotelKat.altaHospedado("Larisa", "Istrate", 60, 50);
			hotelKat.altaHospedado("Juancho", "Marques", 300, 200);
			hotelKat.altaHospedado("Marcos", "Aventin", 170, 302);
			hotelKat.altaHospedado("Gladys", "Bermudez", 154.50, 40);
			// El mejor gatito del mundo, el mio
			hotelKat.altaHospedado("Patch", "Don Gato", 66.90, 37);

			// Creamos un switch para mostrar cada una de las opciones elegidas por el
			// usuario

			switch (op) {

			case 1:
				//Limpiamos el buffer
				input.nextLine();
				
				System.out.println("Damos de alta a un nuevo hospedado");
				System.out.println("Introduce un nombre");
				nombre = input.nextLine();
				
				System.out.println("Introduce un apellido");
				apellido = input.nextLine();
				
				System.out.println("Introduce un precio");
				precio = input.nextDouble();
				
				System.out.println("Introduce un numero de habitacion");
				numHabitacion = input.nextInt();
				
				hotelKat.altaHospedado(nombre, apellido, precio, numHabitacion);
				hotelKat.listaReservas();

				break;

			case 2:
				
				input.nextLine();
				
				System.out.println("Introduce un nombre de la reserva que quieras buscar");
				nombre = input.nextLine();
				System.out.println(hotelKat.buscarReserva(nombre));

				break;

			case 3:
				input.nextLine();
				
				System.out.println("Escriba el nombre de la reseva que deseas eliminar");
				nombre = input.nextLine();
				
				hotelKat.eliminarReserva(nombre);
				hotelKat.listaReservas();

				break;

			case 4:
				
				System.out.println("Listado de las reservas");
				hotelKat.listaReservas();
				
				break;

			case 5:
				input.nextLine();
				System.out.println("Escribe el nombre de la reserva que deseas cambiar");
				nombre = input.nextLine();
				
				System.out.println("Escriba el nuevo nombre de la reserva a cambiar");
				//A�adimos el nuevo nombre que vamos a cambiar
				String nombre2 = input.nextLine();
				
				hotelKat.cambiarReserva(nombre, nombre2);
				hotelKat.listaReservas();
				break;

			case 6:
				
				System.out.println("Listar las habitaciones por rango de precio");
				
				System.out.println("Introduzca el precio minimo");
				int desde = input.nextInt();
				
				System.out.println("Introduzca el precio maximo");
				int hasta = input.nextInt();
				
				hotelKat.listarPorRangoDePrecios(desde, hasta);
				
				break;

			case 7:
				input.nextLine();
				
				System.out.println("Cambio de habitaciones");
				System.out.println("Introduzca el nombre del huesped");
				nombre = input.nextLine();
				System.out.println("Introduzca la habitacion a la cual desea cambiar");
				numHabitacion = input.nextInt();
				
				hotelKat.cambiarHabitacion(nombre, numHabitacion);
				hotelKat.listaReservas();
				
				break;

			case 8:
				
				System.out.println("Incrementamos un 10% en aquellas habitaciones que son pares");
				hotelKat.incrementoDePrecio();
				hotelKat.listaReservas();
				
				break;

			case 9:
				
				System.out.println("Mostramos cuantas habitaciones estan reservadas");
				hotelKat.numeroDeHabitaciones();
				
				break;

			}

		} while (op != 10);

		input.close();

	}
}
